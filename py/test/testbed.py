#!/usr/bin/env python3

import os
import menshnet
import time
import atexit
import logging
import marshal
import json
import requests
import sys

apiKey =sys.argv[1]

# local test options since we are running everything on localstack.
mclient = menshnet.Client(apiKey, loglevel=logging.DEBUG)



def test_event():
    # populate workspace
    mclient.workspace.upload_module("mymodule.py", "./upload.py")

    gimel = mclient.launch()

    def handler(data):
        print("event:" + str(data))

    gimel.register("test-event", handler)
    r = gimel.callmodule("mymodule", "p1", sync=True)
    time.sleep(5)
    mclient.shutdown(gimel)


def test_crash():
    # populate workspace
    mclient.workspace.upload_module("mymodule.py", "./upload.py")

    gimel = mclient.launch()

    def handler(data):
        print(data)

    print("test exception handling")
    r = gimel.callmodule("mymodule", "crash", sync=True)
    time.sleep(5)
    mclient.shutdown(gimel)

def measure_latency():
    # populate workspace
    mclient.workspace.upload_module("mymodule.py", "./upload.py")

    gimel = mclient.launch()
    #atexit.register( lambda *args: mclient.shutdown(gimel) )

    # call echo() 100 times and measure the average round trip time
    # of a call. This is the overhead of moving data through the system.
    round_trips = []
    n = 10
    for i in range(0,n):
        start = time.time()
        r = gimel.callmodule("mymodule", "echo", start, sync=True)
        t = time.time()-start
        round_trips.append( t )
        print("%d rt=%f" % (i,t))  

    round_trips.sort()
    ave = sum(round_trips)/n
    high = round_trips[n-1]
    low = round_trips[0]
    mclient.shutdown(gimel)
    return ave, high, low


def test_stress_test_throughput():
    # populate workspace
    mclient.workspace.upload_module("mymodule.py", "./upload.py")
 
    gimel = mclient.launch()
    #os.system("docker logs -f %s > ./gimel.log &" % gimel.gimelId) 

    atexit.register( lambda *args: mclient.shutdown(gimel) )
    i = 0

    b=time.time()
    n = 300
    for i in range(0,n):
        delay = (n - i) / float(n)
        start = time.time()
        r = gimel.callmodule("mymodule", "echo", "test", sync=True)
        assert(r == "test")
        runtime=time.time()-b  
        print("%f %d: round trip time %f" % (runtime,i, time.time()-start))
        time.sleep(delay)



def test_burst():
    # populate workspace
    mclient.workspace.upload_module("mymodule.py", "./upload.py")
 
    gimel = mclient.launch()

    atexit.register( lambda *args: mclient.shutdown(gimel) )
    n = 100
    for i in range(0,n):
        gimel.callmodule("mymodule", "echo", "test")
    time.sleep(5)    

def test_longrunning():
    # populate workspace
    mclient.workspace.upload_module("mymodule.py", "./upload.py")
 
    gimel = mclient.launch()
    
    def handler(data):
        print(data)
    gimel.register("test-update", handler)
    gimel.callmodule("mymodule", "longrunning")
    print("sleeping for 600 seconds then shutting down")
    time.sleep(600)
    mclient.shutdown(gimel)
    
def test_flood_protection():
    # populate workspace
    mclient.workspace.upload_module("mymodule.py", "./upload.py")
 
    gimel = mclient.launch()
 
    for i in range(0,1001):
        time.sleep(1)   
        gimel.callmodule("mymodule", "longrunning")
    time.sleep(15)
    mclient.shutdown(gimel)

def test_json_event():
    # populate workspace
    mclient.workspace.upload_module("mymodule.py", "./upload.py")

    gimel = mclient.launch()

    def handler(data):
        print("event:" + str(data))

    gimel.register("test-json", handler)
    r = gimel.callmodule("mymodule", "testjson", sync=True, json=True)
    mclient.shutdown(gimel)
    print("test_json_event complete")

     

if __name__ == '__main__':
    print(mclient.workspace.content())

    ave, high, low = measure_latency()
    print("Latency ave: %f, high %f, low %f" % (ave,high,low))

    #test_flood_protection()
    #test_longrunning()
    #test_event()
    #test_json_event()
    #test_crash( )
    #test_stress_test_throughput()
    #test_burst()








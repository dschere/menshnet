#!/usr/bin/env python3

import os
import menshnet
import time
import atexit
import logging
import marshal
import json
import requests

text = open("/home/david/menshnet/data/usr/fakeUID/config.json").read()
apiKey = json.loads(text).get('apiKey')


# local test options since we are running everything on localstack.
mclient = menshnet.Client(apiKey, 
    loglevel=logging.DEBUG, host="192.168.1.153:6000", proto="http")

def test_clone_and_call():
    # populate workspace
    print("cloning")
    mclient.workspace.clone_git(\
        "https://bitbucket.org/dschere/menshnet.git","master")
    # call function and get result
    gimel = mclient.launch()
    os.system("docker logs -f %s > ./gimel.log &" % gimel.gimelId) 
    print("calling upload within git")
    start = time.time()
    r = gimel.callgitmodule("menshnet", "upload", "echo", "test",
        sync=True, 
        path="py/test"
    ) 
    #r = gimel.callmodule("mymodule", "echo", "test", sync=True)
    print("received '%s' rountrip time %f" % (r, time.time()-start))
    assert(r == "test")

    mclient.shutdown(gimel)


def test_event():
    # populate workspace
    mclient.workspace.upload_module("mymodule.py", "./upload.py")

    gimel = mclient.launch()

    def handler(data):
        print("event:" + str(data))

    gimel.register("test-event", handler)
    r = gimel.callmodule("mymodule", "p1", sync=True)
    time.sleep(5)
    mclient.shutdown(gimel)


def test_crash():
    # populate workspace
    mclient.workspace.upload_module("mymodule.py", "./upload.py")

    gimel = mclient.launch()

    def handler(data):
        print(data)

    print("test exception handling")
    r = gimel.callmodule("mymodule", "crash", sync=True)
    time.sleep(5)
    mclient.shutdown(gimel)


def test_stress_test_throughput():
    # populate workspace
    mclient.workspace.upload_module("mymodule.py", "./upload.py")
 
    gimel = mclient.launch()
    #os.system("docker logs -f %s > ./gimel.log &" % gimel.gimelId) 

    atexit.register( lambda *args: mclient.shutdown(gimel) )
    i = 0

    b=time.time()
    n = 300
    for i in range(0,n):
        delay = (n - i) / float(n)
        start = time.time()
        r = gimel.callmodule("mymodule", "echo", "test", sync=True)
        assert(r == "test")
        runtime=time.time()-b  
        print("%f %d: round trip time %f" % (runtime,i, time.time()-start))
        time.sleep(delay)



def test_burst():
    # populate workspace
    mclient.workspace.upload_module("mymodule.py", "./upload.py")
 
    gimel = mclient.launch()
    #os.system("docker logs -f %s > ./gimel.log &" % gimel.gimelId) 

    atexit.register( lambda *args: mclient.shutdown(gimel) )
    n = 100
    for i in range(0,n):
        gimel.callmodule("mymodule", "echo", "test")
    time.sleep(5)    

def test_longrunning():
    # populate workspace
    mclient.workspace.upload_module("mymodule.py", "./upload.py")
 
    gimel = mclient.launch()
    
    def handler(data):
        print(data)
    gimel.register("test-update", handler)
    gimel.callmodule("mymodule", "longrunning")
    print("sleeping for 600 seconds then shutting down")
    time.sleep(600)
    mclient.shutdown(gimel)
    
def test_flood_protection():
    # populate workspace
    mclient.workspace.upload_module("mymodule.py", "./upload.py")
 
    gimel = mclient.launch()
 
    for i in range(0,1001):
        time.sleep(1)   
        gimel.callmodule("mymodule", "longrunning")
    time.sleep(15)
    mclient.shutdown(gimel)

def test_json_event():
    # populate workspace
    mclient.workspace.upload_module("mymodule.py", "./upload.py")

    gimel = mclient.launch()

    def handler(data):
        print("event:" + str(data))

    gimel.register("test-json", handler)
    r = gimel.callmodule("mymodule", "testjson", sync=True, json=True)
    mclient.shutdown(gimel)
    print("test_json_event complete")

def test_no_heartbeat():
    r = requests.get("http://192.168.1.153:6000/api/gimel?apiKey="+apiKey)
    gimelId = r.content.decode()
    print("got gimelId %s, waiting 90s to see if core deletes this gimel" % gimelId)
    time.sleep(90)
    os.system("docker ps -a | grep %s" % gimelId)
     

if __name__ == '__main__':
    test_flood_protection()
    test_longrunning()
    test_clone_and_call()
    test_event()
    test_json_event()
    test_crash() 
    test_stress_test_throughput()
    test_burst()
    test_no_heartbeat()








"""
This is a test module that will be uploaded to a fake account
for testing to validate the menshnet client and server side 
interface.
"""
import traceback
import struct
import time

def echo(data):
    """ varify that the menshnet client can receive the result 
         of a function call
    """
    return data

def p1():
    """
    see if the menshnet global object exists
    """
    gimel.event.emit("test-event", struct.pack('>I',0x1234))
    time.sleep(3)
    gimel.log.info("p1 called")  
    time.sleep(3)
    return 1

def crash():
    """
    force an exception to test the exception handling
    """
    raise RuntimeError("test exception handling")


def longrunning():
    for i in range(0,600):
        time.sleep(1)
        # send an update every second 
        gimel.log.info("sending %d" % i)  
        gimel.event.emit("test-update", i)
    return 0

def testjson():
    gimel.event.emit_json('test-json', {'aaa':90})
    return False



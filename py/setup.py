#!/usr/bin/env python3

from setuptools import setup
#from distutils.core import setup
import os.path




setup(name='menshnet',
      version='0.8',
      description='Hardware acceleration as a service',
      long_description="""
See https://bitbucket.org/dschere/menshnet/src/master/

Menshnet client interface. This is a library that allows a registered
user the ability to upload arithmatically intensive python code to 
the menshnet hardware acceleration architecture and execute them remotely
thereby cheaply reaping the benefits of FPGA based hardware. 

Currently, there are two supported languages for this client:
python and javascript located in the js and py subdirectories respectively.
""",
      author='David Schere',
      author_email='eagle@lightinlogic.com',
      url='https://menshnet.online',
      packages=['menshnet'],
      install_requires=["paho-mqtt","requests"],
      license="MIT",
      classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3"
      ],
     )



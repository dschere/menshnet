

def crash():

    def level2():
        raise RuntimeError("test raising exceptions in a gimel")

    def level1():
        return level2()

    return level1()


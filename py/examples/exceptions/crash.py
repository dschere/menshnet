#!/usr/bin/env python3

import pathlib
import sys
import atexit

# The thin client for menshnet.online
import menshnet


# create communication to menshnet.
apiKey = sys.argv[1] 
mclient = menshnet.Client(apiKey)

path = str(pathlib.Path().absolute())+"/crash_gimel.py"
print("Uploading file to your workspace")
mclient.workspace.upload_module("crash_gimel.py", path)

gimel = mclient.launch()

atexit.register(lambda *args: mclient.shutdown(gimel))

print("call a gimel that relays the exception to client") 
try:  
    gimel.callmodule("crash_gimel", "crash", sync=True)
except menshnet.exceptions.MenshnetGimelError:
    print("Success, exception was relayed back to client")







import cv2


def detectFaces(filename):
    gimel.log.info("inside %s.%s(%s)" % (__file__,"detectFaces",filename))

    # Load the cascade
    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

    # Read the input image
    #img = cv2.imread('test.jpg')
    img = cv2.imread(filename)
    gimel.log.info("opened file")

    # Convert into grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Detect faces
    faces = face_cascade.detectMultiScale(gray, 1.1, 4)
    gimel.log.info("detected %d faces" % len(faces))

    # Draw rectangle around the faces
    for (x, y, w, h) in faces:
        gimel.event.emit("face-rectangle", {
            "left-top": (x,y),
            "right-bottom": (x + w, y + h)
        })

    return len(faces)     


def unittest():
    global gimel

    # Uncomment this if you are linting or unit testing this file.
    # this is the interface to the gimel object.
    class MockEventEmitter:
        def emit_json(self, n, data):
            import json
            # ensure it can be serialized.
            json.dumps(data)
        def emit(self, n, data):
            import pickle
            pickle.dumps(data)                       
    class MockGimel:
        def __init__(self):
            self.event = MockEventEmitter()
            import logging
            # ensure it can be serialized.
            self.log = logging
    gimel = MockGimel()

    r=detectFaces("test.jpg")
    print(r)


if __name__ == '__main__':
    unittest()




#!/usr/bin/env python3

import time
import sys
import atexit

# The thin client for menshnet.online
import menshnet




# create communication to menshnet.
apiKey = sys.argv[1] # menshnet.online->login->dashboard->cut and paste API_KEY from upper left.
mclient = menshnet.Client(apiKey)


giturl = "https://dschere@bitbucket.org/dschere/menshnet.git"
branch = "master"
print("Cloning git repo into our workspace")
mclient.workspace.clone_git(giturl, branch)

print("creating gimel")
gimel_agent = mclient.launch()


atexit.register(lambda *args: mclient.shutdown(gimel_agent))


# register event handler 
gimel_agent.register("face-rectangle", lambda data: print(str(data)))


print("calling detect_face_gimel from our workspace")
def result_handler(result):
     print("async handler for result received %s" % str(result))

# call asynchronously and pass an argument to our gimel function
# cd <workspace>/git/<reponame>/<path> 
# import detect_face_gimel
# call detect_face_gimel.detectfaces("test.jpg") asynchronously      
gimel_agent.callgitmodule("menshnet", "detect_face_gimel", "detectFaces", "test.jpg", 
    path="py/examples/facedetect", async_result_handler=result_handler)

print("sleep in main thread for 10 seconds")
time.sleep(10)







#!/usr/bin/env python3
"""
Launch an opencv application which does a dense optical flow
on a demo video and routes motion vectors accross the newtork
to this app.
"""


import pathlib
import sys
# so that pickle can load remote object. 
import numpy as np


# The thin client for menshnet.online
import menshnet


"""
Goto menshnet.online, login, click on dashboard. The API_KEY is 
in the upper left.
"""

# create communication to menshnet.
apiKey = sys.argv[1] 
mclient = menshnet.Client(apiKey)

# upload module
path = str(pathlib.Path().absolute())+"/events_gimel.py"
print("Uploading file to your workspace, and call it optical_flow.py in our workspace")
mclient.workspace.upload_module("optical_flow.py", path)

# start our gimel process
print("starting gimel ...")
client_gimel = mclient.launch()

# Cache the "import ..." operations in optical_flow so that the first call
# our remote procedure doesn't have to wait to import opencv and numpy
print("cache importing opencv and numpy") 
client_gimel.cacheModuleImports("optical_flow")


# capture events ...
def hsv_handler(event):
    print("hsv numpy array")
    print(event)
client_gimel.register("hsv",hsv_handler) 

# block in main thread, handle events in the menshnet background thread.
print("connect and process 16 frames of video then exit")
result = client_gimel.callmodule("optical_flow", "denseOpticalFlow", sync=True)
print("result = " + str(result))

mclient.shutdown(client_gimel)






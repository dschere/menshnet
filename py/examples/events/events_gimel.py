"""
This demo introduces the global ‘gimel’ object 

# send an event to the client
gimel.event.emit(eventId, data)
      eventId - arbitrary unique string id that the menshnet client library has
                     Registered to capture events.
      data    - anything that python pickle can serialize.

^^^^^
  The menshnet client can capture events using 
  gimel.register(eventId, callable)
      eventId  - arbitrary unique string id that the menshnet client library has
                 Registered to capture events.
      callable - function that accepts data as an argument. So 
                 >>> gimel.event.emit("foo", "bar") 
                 def callable(data):
                     data <-- "bar"


gimel.event.emit_json(eventId, data)
      eventId - arbitrary unique string id that the menshnet client library has
                     Registered to capture events.
      data      - anything that json can serialize.
^^^^^^^

    Same as gimel.event.emit only json is used as the serialization format. Obviously
    that implied data must be compatable with json.dumps


gimel.log

    This is log router that routes log messages to the menshnet client's logger.
    so gimel.log.info("hello") results in client application calling this gimel
    will have "hello" in its logs.

 
"""
import time
import io
import base64

# Uncomment this if you are linting or unit testing this file.
# this is the interface to the gimel object.
"""
class MockEventEmitter:
    def emit_json(self, n, data):
        import json
        # ensure it can be serialized.
        json.dumps(data)
    def emit(self, n, data):
        import pickle
        pickle.dumps(data)                       
class MockGimel:
    def __init__(self):
        self.event = MockEventEmitter()
        import logging
        # ensure it can be serialized.
        self.log = logging
gimel = MockGimel()
"""

# Free traffic video 
# URL="rtmp://strmr5.sha.maryland.gov/rtplive/3200f6fe0080000b005dd336c4235c0a"
URL="https://player.vimeo.com/external/328732843.sd.mp4?s=99ae36e251bca106e42a15c856aad51a9704d3c2&profile_id=164&oauth2_token_id=57447761"

import cv2 as cv
import numpy as np



def denseOpticalFlow():
    """ Typical of the operation that one would use opencv for.
        Optical flow is one of the core routines in the toolbag
        for object tracking
    """
    gimel.log.info("connecting to url")

    cap = cv.VideoCapture(URL)
    ret, frame1 = cap.read()

    gimel.log.info("processing first frame")

    # resize to quater cif for viewing in browser.
    dim = (352,240)
    frame1 = cv.resize(frame1, dim, interpolation = cv.INTER_AREA)

    prvs = cv.cvtColor(frame1,cv.COLOR_BGR2GRAY)
    hsv = np.zeros_like(frame1)
    hsv[...,1] = 255
    for frame_number in range(0,16):
        ret, frame2 = cap.read()
        if ret == False:
            gimel.log.info("finished")
            break
            
        frame2 = cv.resize(frame2, dim, interpolation = cv.INTER_AREA)

        next = cv.cvtColor(frame2,cv.COLOR_BGR2GRAY)
        flow = cv.calcOpticalFlowFarneback(prvs,next, None, 0.5, 3, 15, 3, 5, 1.2, 0)
        mag, ang = cv.cartToPolar(flow[...,0], flow[...,1])
        hsv[...,0] = ang*180/np.pi/2
        hsv[...,2] = cv.normalize(mag,None,0,255,cv.NORM_MINMAX)

        # send numpy array to client
        gimel.event.emit("hsv", hsv) 

    return True    


        

 










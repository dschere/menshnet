#!/usr/bin/env python3

import pathlib
import sys


# The thin client for menshnet.online
import menshnet


"""
Goto menshnet.online, login, click on dashboard. The API_KEY is 
in the upper left.
"""

# create communication to menshnet.
apiKey = sys.argv[1] 
mclient = menshnet.Client(apiKey)

# populate user workspace which is a directory structure
#   /modules
#   /git
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# NOTE, you only need to do this once to upload the module (or update it)
path = str(pathlib.Path().absolute())+"/hello.py"
print("Uploading file to your workspace")
mclient.workspace.upload_module("hello.py", path)

# launch a remote process with the menshnet cluster to execute
# rotines. The gimel object returned by launch is now linked to the 
# remote process. When the gimel object is deallocated the remote process
# dies. While it remains heartbeat messages will be sent to keep the remote
# process alive.
print("""
Step 1:
    Launching remote 'Gimel' process. This process is tied to the lifecycle of
    gimel object in python returned by the following statement:

    gimel = mclient.launch()

    When 'gimel' is garbage collected on the client side or if communications are
    lost then gimel process on menshnet is deallocated.

    You can make as many calls to the gimel as you like.
""")
gimel = mclient.launch()

 

print("""
Step 2:
    Using the simpliest method: Calling remote function helloword() synchronously.

    def helloworld():
        return "Hello World"

    and returning result back to client.
""")
# simpliest call. If sync=True then the function will block until 
# mymodule.dosomething() returns. 
result = gimel.callmodule("hello", "helloworld", sync=True)

assert result == "Hello World"
print("Worked!!!!!  Got result >>>> " + result)


print("""
Optional Step:

Graceful shutdown of remote process.
""")
# when you are finished. If the program dies then the gimel(s) launched
# will also die with 60 seconds since no heartbeat messages will sent.
mclient.shutdown(gimel)













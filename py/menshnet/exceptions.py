"""
Exceptions that are thown by this library.
"""


class MenshnetError(RuntimeError):
    """
    Based catch all error  
    """

class MenshnetGimelError(RuntimeError):
    """
    Exception occured when making a call to a gimel  
    """

class MenshnetRemoteCallTimeout(MenshnetError):
    """
    timeout waiting on remote procedure call 
    """

class UserHandlerException(MenshnetError):
    """
    exception in user callback
    """

class MenshnetServerTimeout(MenshnetError):
    """
    network timeout
    """

class MenshnetServerError(MenshnetError):
    """
    Internal server error
    """





import paho.mqtt.client as mqtt
import threading
import unittest
import os
import time
import traceback


class Messenger:
    """
    This object manages an mqtt client and a collection of topic handlers.
    """

    def __init__(self, handlers, exc_handler, log):
        self.handlers = handlers
        self.exc_handler = exc_handler
        self.log = log
        self.ready = threading.Event()
        self.awaiting_subscribed = set()
        self.client = mqtt.Client(transport='websockets')

    def subscribe(self, topic, cb, timeout=10):
        self.handlers[topic] = cb
        self.ready.clear()
        (_, mid) = self.client.subscribe(topic)
        self.awaiting_subscribed.add(mid)
        return self.ready.wait(timeout)

    def unsubscribe(self, topic):
        if topic in self.handlers:
            del self.handlers[topic]
            self.client.unsubscribe(topic)

    def connect(self, addr, timeout):
        """
        connect to message broker and subscribe to topics
        block until this is completed.

        return true if connected.
        """
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.client.on_subscribe = self.on_subscribe

        try:
            self.client.connect(addr, 9001, 60)
        except BaseException:
            return False
        self.client.loop_start()

        # wait for communication to be established.
        return self.ready.wait(timeout)

    def on_subscribe(self, client, userdata, mid, granted_qos):
        """ handle subscribe events from mqtt, this will set the
            ready mutex if all rrequested subscriptions are complete.
        """
        if mid in self.awaiting_subscribed:
            self.awaiting_subscribed.remove(mid)

        if len(self.awaiting_subscribed) == 0:
            self.ready.set()

    def publish(self, topic, data):
        " publish message "
        self.client.publish(topic, payload=data)

    # The callback for when the client receives a CONNACK response from the
    # server.
    def on_connect(self, client, userdata, flags, rc):
        "when connected subscribe to all requested topics"
        self.log.debug("connected to menshnet, subscribing to topics")
        if rc == 0:
            for topic in self.handlers:
                (_, mid) = client.subscribe(topic)
                self.awaiting_subscribed.add(mid)
            if len(self.awaiting_subscribed) == 0:
                self.ready.set()

    # The callback for when a PUBLISH message is received from the server.
    def on_message(self, client, userdata, msg):
        "route inbound message"
        self.log.debug("received message from topic %s" % msg.topic)
        cb = self.handlers.get(msg.topic)
        if cb:
            try:
                # user defined processing function
                cb(client, msg.payload)
            except BaseException:
                stack_trace = traceback.format_exc()
                self.exc_handler(stack_trace)


"""
Object controls the lifecycle of a remote gimel microservice.

The remote service is created when the object is created and destroyed
when the object is deleted or if the application is shutdown. The
gimel agent sends periodic keep alives so if the application is killed
non gracefully the remote gimel microservice shall eventually be stopped
after 1 minute.
"""
import threading
import requests
import json
import logging
import pickle
import uuid
import queue
import traceback

import menshnet.exceptions as exceptions
import menshnet.messenger as messenger 


class GimelAgent:
    """
    This object controls the lifecycle and actions of the
    remote gimel microservice.
    """


    def _remote_log(self, mqtt_client, payload):
        data = json.loads(payload.decode())
        if callable(self.mci.gimel_log_handler):
            self.mci.gimel_log_handler(\
                data['timestamp'], data['severity'], data['msg'])
        else:
            msg = "%(timestamp)f %(severity)s %(msg)s" % data
            func = getattr(self.mci.log, data['severity'])
            func(msg)
        
    def _route_event(self, client, payload):
        """
        route event associated with a key value to a user
        defined event handler.
        """
        encoding_id = payload[0]
        if encoding_id == ord('{'):
            data = json.loads(payload.decode())
        else:
            data = pickle.loads(payload)
        cb = self.evt_handler.get(data['key'])
        if cb:
            cb(data)

    def exc_handler(self, data):
        """ exception received from remote gimel.
        """
        if callable(self.mci.gimel_exc_handler):
            stop_gimel = self.mci.gimel_exc_handler(
                data['timestamp'],
                data['stack_trace'],
                data['inbound_message'] 
            )
            if stop_gimel:
                self.mci.shutdown(self)
        else:     
            # default behaviour:
            #   log error and shut down remote gimel (if its running) 
            preamble = "Exception occured in gimel %s:\n" % self.gimelId
            body = json.dumps(data, indent=4, sort_keys=True)
            footer = "All callbacks shall be unregistered, remote gimel is dead"
            self.mci.log.error(preamble + body + footer)
            self.mci.shutdown(self)
            raise exceptions.MenshnetGimelError("Unhandled Exception in Gimel")

    def set_exc_handler(self, cb):
        self.gimel_exc_handler = cb

    def set_user_log_handler(self, cb):
        self.user_log_handler = cb

    def _remote_exc_handler(self, mqtt_client, payload):
        self.exc_handler(json.loads(payload.decode())) 

    def _remote_event_handler(self, mqtt_client, payload):
        encoding_id = payload[0]
        if encoding_id == ord('{'):
            data = json.loads(payload.decode())
        else:
            data = pickle.loads(payload)
        key = data.get('key')
        if key in self.evt_handler:
            handler = self.evt_handler.get(key)
            handler(data)  
        

    def _remote_result(self, mqtt_client, payload):
        """
        route result of rpc call to response handler.

        pickle.dumps -> {
            msgId, result
        }
        """
        try:
            try:
                data = pickle.loads(payload)
            except:
                data = json.loads(payload.decode())
            msgId = data['msgId']
            response = self.method_result_handler.get(msgId)
            
            if response:
                try:
                    response(data)
                except BaseException:
                    raise exceptions.UserHandlerException
                del self.method_result_handler[msgId]
        except:
            print(traceback.format_exc())
            

    def __init__(self, gimelId, mci):
        self.mci = mci         # menshnet client instance
        self.gimelId = gimelId  # generated uid for backend microservice
        #self.exc_handler = self.default_exc_handler
        
        topic_prefix = "/api/gimelhost/" + gimelId
        topic_log = topic_prefix + "/log"
        topic_emit = topic_prefix + "/emit"
        topic_exc = topic_prefix + "/user_code_exception"
        topic_result = topic_prefix + "/result"
        topic_event = topic_prefix + "/event"

        topic_handlers = {
            topic_log: self._remote_log,
            topic_result: self._remote_result,
            topic_exc: self._remote_exc_handler,
            topic_event: self._remote_event_handler
        }
        self.evt_handler = {}
        self.method_result_handler = {}

        # connect to menshnet's message broker to receive live
        # events from gimel
        self.m = messenger.Messenger(
            topic_handlers, self.exc_handler, self.mci.log)
        
        # block till connected ...
        addr = mci.config.host.split(':')[0]

        connected = self.m.connect(addr, mci.config.timeout)
        if not connected:
            msg = "Unable to connect to '%s' after %s seconds" % (
                mci.config.host, str(mci.config.timeout)
            )
            raise exceptions.MenshnetServerTimeout(msg)

    def register(self, event, handler):
        if not callable(handler):
            raise TypeError("Event handler must be callable")
        self.evt_handler[event] = handler

    def unregister(self, event):
        if event in self.evt_handler:
            del self.evt_handler[event]

    def cacheModuleImports(self, modname, **opts):
        """ have the module loaded in the gimel without execution this allows
            for 'import ...' statements to be executed speeding up subsiquent
            calls to callmodule

            Note: this generates $SYS/import_cache user events that can be logged

            returns the elapsed time to execute 'import ...' statements 
        """
        return self.callmodule(modname, "__import_cache__", sync=True)

    def callmodule(self, modname, funcname, *args, **opts):
        return self._call("/modules", modname,
                          funcname, *args, **opts)

    def cacheGitModuleImports(self, reponame, modname, **opts):
        """ have the module loaded in the gimel without execution this allows
            for 'import ...' statements to be executed speeding up subsiquent
            calls to callgitmodule
        """
        return self.callgitmodule(reponame, modname, "__import_cache__", sync=True)


    def callgitmodule(self, reponame, modname, funcname, *args, **opts):
        path = "/git/" + reponame
        if 'path' in opts:
            p = opts['path']
            if len(p) > 0 and p[0] == '/':
                p = p[1:]
            if len(p) > 0 and p[-1] == '/':
                p = p[:-1]
            path += "/" + p
            del opts['path']
        return self._call(path, modname, funcname, *args, **opts)

    def _call(self, path, modname, objname, *args, **opts):
        """
        Primitive used to call function hosted within menshnet running
        within a cluster.

        To run a module function

        call("<module name>","<func>", ..., module=True)

        To run a function within a git repo
        call("/path/to/file/","<func>", ..., git=True)

        opts:

            sync: block timeout seconds until remote call completes and return
                  results from that remote call from this function.
            async_result_handler:
                  if provided call the result_handler with the result
                  of the remote function call. do not block.

            neither of these being set is a send and forget remote call.
        """
        msgId = None  # default is send and forget, don't care about response.
        response_queue = None

        sync_call = opts.get('sync', False)
        async_result_handler = opts.get('async_result_handler', None)

        if sync_call and async_result_handler:
            errmsg = "sync and async_result_handler are mutually exclusive!"
            raise exceptions.MenshnetError(errmsg)

        if sync_call:
            msgId = str(uuid.uuid4())
            response_queue = queue.Queue()
            self.method_result_handler[msgId] = \
                lambda data: response_queue.put(data)
        elif async_result_handler:
            msgId = str(uuid.uuid4())
            self.method_result_handler[msgId] = async_result_handler

        proto = self.mci.config.proto
        host = self.mci.config.host
        apiKey = self.mci.apiKey
        #urlfmt = "%(proto)s://%(host)s/api/gimel/run?apiKey=%(apiKey)s"
        urlfmt = "%(proto)s://%(host)s/api/gimel/run"
        url = urlfmt % locals()

        r = requests.post(url, json={
            "apiKey": self.mci.apiKey,
            "gimelId": self.gimelId,
            "modname": modname,
            "path": path,
            "objname": objname,
            "args": args,
            "msgId": msgId,
            "json": opts.get('json','False')
        })
        if r.status_code != 200:
            err = "Unable to call remote function, HTTP error %d, %s"
            errmsg = err % (r.status_code, r.content)
            self.mci.log.error(errmsg)
            raise exceptions.MenshnetError(errmsg)

        if sync_call:
            result_timeout = opts.get('result_timeout', 60)
            try:
                data = response_queue.get(timeout=result_timeout)
                error = data.get('error')
                if error:
                    raise exceptions.MenshnetGimelError(error)
                return data['result']     
            except queue.Empty:
                errmsg = "Timeout waiting for response"
                raise exceptions.MenshnetRemoteCallTimeout(errmsg)

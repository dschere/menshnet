"""
Menshnet client interface. This is a library that allows a registered
user the ability to upload arithmatically intensive python code to 
the menshnet hardware acceleration architecture and execute them remotely
thereby cheaply reaping the benefits of FPGA based hardware. 

"""
import requests
import logging
import sys
import time
import threading
import queue
import atexit

import menshnet.gimel as gimel
import menshnet.exceptions as exceptions
import menshnet.workspace as workspace


class MenshnetClientConfigOpts:
    # maximum amount fo time to connect to menshnet.online before timeout
    timeout = 10

    # allow this to be replaced for testing perposes or perhaps the end
    # userwould have to setup a proxy to get out.
    host = "menshnet.online"
    proto = "https"

    def __init__(self, opts):
        for k in opts:
            setattr(self,k,opts[k])


class LifecycleMngr(threading.Thread):
    """
    <b>NOTE: This is an internal class that is not part of the API</b>

    Implements an damon thread that does the following:

    <ul>
    <li>send periodic keep alives for running gimels.
    <li>send immediate delete commands to shut down gimels
    <li>shutdown all running gimels on program exit 
    </ul>

    <p>
    Note: There is a server side failsafe in that all gimels
    will be stopped after 1 minute if this thread is not able to send
    a keeplive due to network outage.   
    </p>
    """
    

    def __init__(self, mci):
        threading.Thread.__init__(self)

        self.mci = mci
        self.msgq = queue.Queue()
        self.daemon = True
        self.running = threading.Event()
        self.running.set()
        self.gimels = set()
        self.heartbeat_interval = 15
        self.next_heartbeat = time.time() + self.heartbeat_interval

        # on shutdown cycle through all running gimels and do a graceful 
        # exit. 
        atexit.register(self.on_shutdown)

    def on_shutdown(self):
        for gimel in list(self.gimels):
            self._shutdown([gimel])

    def stop(self):
        "Stop thread"
        self.msgq.put(("stop", None))

    def shutdown(self, gimelInst):
        "Shutdown server side service for gimel"
        if gimelInst in self.gimels:
            self.msgq.put(("shutdown", [gimelInst]))

    def add(self, gimelInst):
        "trigger the self._add function to be called inside the thread"
        self.msgq.put(("add", [gimelInst]))

    def _add(self, args):
        gimelInst = args[0]
        self.gimels.add(gimelInst)

    def _shutdown(self, args):
        gimelInst = args[0]
 
        proto = self.mci.config.proto
        host = self.mci.config.host
        gimelId = gimelInst.gimelId
        apiKey = self.mci.apiKey
        urlfmt = "%(proto)s://%(host)s/api/gimel?gimelId=%(gimelId)s&apiKey=%(apiKey)s"
        url = urlfmt % locals()
        try:
            r = requests.delete(url, timeout=self.mci.config.timeout)
        except requests.exceptions.ConnectTimeout:
            self.mci.log.error("Timeout while trying to stop gimel, " +
                               "server shall eventually stop gimel if no keep alive is sent.")
            return

        if gimelInst in self.gimels:
            self.gimels.remove(gimelInst)
         

    def _background(self):
        """ this is called after either a timeout or event to perform background
            tasks.  
        """
        proto = self.mci.config.proto
        apiKey = self.mci.apiKey
        now = time.time()
        # is it time yet to send hearbeats?
        if now < self.next_heartbeat:
            # No .. return
            return

        if len(self.gimels) == 0:
            # no gimels being managed.
            return  

        # update time for next one
        self.next_heartbeat = now + self.heartbeat_interval

        host = self.mci.config.host
        urlfmt = "%(proto)s://%(host)s/api/gimel-heartbeat?apiKey=%(apiKey)s"
        url = urlfmt % locals()
        try:
            r = requests.post(url, json={
                "gimelIdList": [g.gimelId for g in self.gimels]
            })
        except requests.exceptions.ConnectTimeout:
            msg = "Timeout while trying to send heartbeat for gimel, " +\
                "server shall eventually stop gimel if no keep alive is sent."
            self.mci.log.error(msg)
            return


    def run(self):
        """
        Thread loop.
        """
        dispatch = {
            "add": self._add,
            "shutdown": self._shutdown,
            "stop": self._stop
        }
        n = self.heartbeat_interval
        while self.running.is_set():
            try:
                (cmd, args) = self.msgq.get(timeout=n)
            except queue.Empty:
                (cmd, args) = ("noop", None)
            f = dispatch.get(cmd)
            if f:
                f(args)
            self._background()


class MenshnetClient:
    """
    <b>Main class for end user API.</b>

    
     

    


    """

    def _create_logger(self, loglevel):
        """Create a default logger if user doesn't specify one.
        """
        fmt = "%(asctime)s %(message)s"

        logger = logging.getLogger("MenshnetClient")

        console_handler = logging.StreamHandler(stream=sys.stdout)
        console_handler.setFormatter(logging.Formatter(fmt))

        logger.addHandler(console_handler)
        logger.setLevel(loglevel)

        return logger

    def __init__(self, apiKey, **opts):
        """
        Note: This class is renamed to 'Client' by __init__.py so
              it is invoked as follows:
        
        menshnet.Client(apikey, **opts)     

        apiKey: required security token provided by menshnet.online
                it is available via login->dashboard displayed in the 
                upper left.

        opts:    

        Option             | Description        
        ------------------ | ------------------------------------------------------------
        loglevel           | loglevel as defined in the the logging module
                           | defaults to logging.INFO
                           | Example: 
                           |     import loglevel, menshnet
                           |
        logger             | User supplied logging object, defaults to a console logger.
                           | that logs to stdout.
                           |
        gimel_exc_handler  | If provided this will override the default behavior if
                           | an unhandled exception occured in the gimel:
                           | 1. log the exception
                           | 2. kill the gimel process
                           | 3. raise a exceptions.MenshnetGimelError
                           | To override this provide a callback 
                           |   gimel_exc_handler(timestamp, stacktrace, msg)
                           |    - timestamp :  time.clock_gettime(time.CLOCK_REALTIME)
                           |    - stacktrace:  string created by traceback.format_exc()
                           |    - msg       :  the message sent to the gimel that called
                           |                  the function that caused the exception.
                           | 
        gimel_log_handler  | If provided logging events from gimel.log.<severity> routed to 
                           | this callback  rather than the default behavior which is to 
                           | route them to the logger object.
                           | gimel_log_handler(timestamp, severity, msg)
                           |   - timestamp :  time.clock_gettime(time.CLOCK_REALTIME)
                           |   - severity  :  "debug","info","warning","error"
                           |   - msg       :  logging text
                           |  


         Examples:   
         =========          

         import menshnet

         # normal use case using all defaults.

         mc = menshnet.Client(apiKey)
         

         def my_exc_handler(timestamp,stacktrace,msg):
             pass

         def gimel_log_handler(timestamp, severity, msg):
             pass
         
         # override logging and exception handling

         mc = menshnet.Client(apiKey, 
             gimel_exc_handler=my_exc_handler,
             gimel_log_handler) 

         # override log level of gimel.log.<severity>() messages

         mc = menshnet.Client(apiKey,loglevel=logging.DEBUG)
            
        """
        self.opts = opts

        loglevel = opts.get('loglevel', logging.INFO)
        self.log = opts.get('logger', self._create_logger(loglevel))
        self.gimel_exc_handler = opts.get('gimel_exc_handler')
        self.gimel_log_handler = opts.get('gimel_log_handler')

        self.apiKey = apiKey
        self.config = MenshnetClientConfigOpts(opts)
        self.lifecycle_manager = LifecycleMngr(self)
        self.lifecycle_manager.start()
        self.workspace = workspace.Workspace(self)

    def shutdown(self, gimelInst):
        if not isinstance(gimelInst, gimel.GimelAgent):
            raise TypeError("Expected gimel agent return by launch method")
        self.lifecycle_manager.shutdown(gimelInst)

    def launch(self):
        """
        Provisions a microseverice remotely that is controlled by
        the returned GimelAgent object. The microservice is bound to 
        the lifecycle of the GimelAgent, the service will run until the
        GimeAgent is destroyed by being passed to teh shutdown function
        or if the application goes down.    
        """
        proto = self.config.proto
        host = self.config.host
        apiKey = self.apiKey
        urlfmt = "%(proto)s://%(host)s/api/gimel?apiKey=%(apiKey)s"
        url = urlfmt % locals()
        try:
            res = requests.get(url, timeout=self.config.timeout)
        except requests.exceptions.ConnectTimeout:
            msg = "Unable to connect to '%s' in %s seconds" % (
                host, str(self.config.timeout)
            )
            raise exceptions.MenshnetServerTimeout(msg)
        if res.status_code != 200:
            raise exceptions.MenshnetServerError(res.content)

        # create object to give to user to manipulate thier gimel
        gimelId = res.content.decode()
        gimelInst = gimel.GimelAgent(gimelId, self)
        self.lifecycle_manager.add(gimelInst)

        return gimelInst

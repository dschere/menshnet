"""
Control the contents of the workspace. Given that this
is happening outside of the UI generating an apiKey is
not allowed.
"""
import json
import requests
import menshnet.exceptions as exceptions


class Workspace:
    def __init__(self, mci):
        self.mci = mci

    def content(self):
        """
        Returns a presentation of the user workspace 
        {
            "git": [{
                "branch": <branch name>,
                "url": <git url that will be cloned>
            }, ...],
            "modules":[
                "<module1>.py", ....
            ]  
        }   
        """
        proto = self.mci.config.proto
        host = self.mci.config.host
        apiKey = self.mci.apiKey
        urlfmt = "%(proto)s://%(host)s/api/workspace?apiKey=%(apiKey)s"
        url = urlfmt % locals()
        
        try:
            r = requests.get(url, timeout=10)
        except:
            errmsg = "Error while trying to get workspace content"
            self.mci.log.error(errmsg)
            raise exceptions.MenshnetServerTimeout(errmsg)

        if r.status_code != 200:
            errargs = (r.status_code, str(r.content))
            errmsg = "HTTP error %d, %s" % errargs
            self.mci.log.error(errmsg)
            raise exceptions.MenshnetError(errmsg)

        # decode json and return 
        return json.loads(r.content.decode()) 


    def clone_git(self, giturl, branch, passwordOrAccessToken=None):
        """
        Clone git repo into workspace.
        """
        proto = self.mci.config.proto
        host = self.mci.config.host
        apiKey = self.mci.apiKey
        urlfmt = "%(proto)s://%(host)s/api/git?apiKey=%(apiKey)s"
        url = urlfmt % locals()
        self.mci.log.info(url)
        try:
            r = requests.post(url, json={
                "gitUrl": giturl,
                "branch": branch,
                "auth": passwordOrAccessToken is not None,
                "password": passwordOrAccessToken
            })
        except BaseException:
            errmsg = "Timeout while trying to upload git %s" % giturl
            self.mci.log.error(errmsg)
            raise exceptions.MenshnetServerTimeout(errmsg)

        if r.status_code != 200:
            errargs = (giturl, r.status_code, str(r.content))
            errmsg = "Menshnet attempted to clone %s: HTTP error %d, %s" % errargs
            self.mci.log.error(errmsg)
            raise exceptions.MenshnetError(errmsg)

    def upload_module(self, pymodname, localpath):
        """
        Upload module to workspace.
        """
        multipart_form_data = {
            'file': (pymodname, open(localpath, 'rb'))
        }

        proto = self.mci.config.proto
        host = self.mci.config.host
        apiKey = self.mci.apiKey
        urlfmt = "%(proto)s://%(host)s/api/module?apiKey=%(apiKey)s"
        url = urlfmt % locals()

        try:
            r = requests.post(url, files=multipart_form_data, timeout=30)
        except BaseException:
            errmsg = "Timeout while trying to upload %s" % localpath
            self.mci.log.error(errmsg)
            raise exceptions.MenshnetServerTimeout(errmsg)

        if r.status_code != 200:
            errargs = (localpath, pymodname, r.status_code, str(r.content))
            errmsg = "While attempting to upload %s as %s: HTTP error %d, %s" % errargs
            self.mci.log.error(errmsg)
            raise exceptions.MenshnetError(errmsg)

# README #

### Menshnet Client ###

Menshnet client interface. This is a library that allows a registered
user the ability to upload arithmatically intensive python code to 
the menshnet hardware acceleration architecture and execute them remotely
thereby cheaply reaping the benefits of FPGA based hardware. 

Currently, there are two supported languages for this client:
python and javascript located in the js and py subdirectories respectively.


### Installation ###

Python
======

   * pip3 install menshnet

Javascript
==========

```
   <!-- if you use a different version of paho-mqtt make sure it supports the 
        connect option 'reconnect' 
    -->
   <script src="https://unpkg.com/paho-mqtt@1.0.4/paho-mqtt.js"></script>
   <script src="path/to/menshnet.js"></script>
```

### Quick Start ###

Here is a quick tutorial:

1. Goto menshnet.online and create an account 
2. Goto menshnet.online and click on 'dashboard'
3. copy the API_KEY in the upper right corner to some safe place
   you will need this for the client.

Python
======

```

# create comminication to menshnet. 
mclient = menshnet.Client(apiKey)

# populate user workspace which is a directory structure
#   /modules
#   /git
mclient.workspace.upload_module("mymodule.py", "/path/to/your/module.py")

# launch a remote process with the menshnet cluster to execute
# rotines. The gimel object returned by launch is now linked to the 
# remote process. When the gimel object is deallocated the remote process
# dies. While it remains heartbeat messages will be sent to keep the remote
# process alive.
gimel = mclient.launch()


# simpliest call. If sync=True then the function will block until 
# mymodule.dosomething() returns. 
result = gimel.callmodule("mymodule", "dosomething", sync=True)

# when you are finished. If the program dies then the gimel(s) launched
# will also die with 60 seconds since no heartbeat messages will sent.
mclient.shutdown(gimel)

```

Javascript
==========

Yes, you can offload work to python code remotely from within the javascript
client side.

```

/**
    Note: The javascript client does not have the ability to upload files 
    or register git repos like the python one does. So you will have to either
    use the dashboard on menshnet.online or the python client to populate
    your remote workspace with code so it can be executed remotely.
*/
Menshnet(apiKey).then((mclient)=>{
    mclient.launch((gimel)=>{
        gimel.callmodule("mymodule","dosomething")
        .then((function_result)=>{
            // mymodule.dosomething() -> function_result
        },(error)=>{
            alert(error);
        }); 
    }, (error)=>{
        alert(error);
    });
}, (error)=> {
    alert(error); 
});

```



Roadmap
=======

Here are some longer term goals:


Offer a C menshnet client so that it can be widely
ported to virtually every language. This should be
done after the menshnet client is widely used the interface
is stable. Once this is implemented then SWIG can be
used to create bindings to many many languages:
http://www.swig.org/compat.html#SupportedLanguages

Offer asyncio-menshnet that will take
advantage of the newer asyncio features in python3.8+ 
 















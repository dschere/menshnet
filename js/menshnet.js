/**
    Menshnet javascript client.

Dependencies:

mqtt:
    https://cdnjs.cloudflare.com/ajax/libs/paho-mqtt/1.0.1/mqttws31.js  

jquery
    https://code.jquery.com/jquery-3.6.0.min.js

This is a small library that provides the ability to start,stop and interact
with gimel functions on menshnet.



var m = Menshnet();
m.connect().then(
     on_connected,
     on_failure    
)

*/

UidCounter = 0;
function uid() { 
    var salt = Math.random().toString(16).substr(2, 8);
    var d = new Date();
    UidCounter++;
    return UidCounter + '.' + salt + "-" + d.getTime();
}




class Gimel
{
    /**
       created by menshnet client. 
       * sends periodic heartbeat message to keep gimel 
         process alive server side.
       * interface to allow users to call remote functions
       * interface to allow users to route events from remote
         functions. 
    */
    constructor(gimelId, apiKey) {

        // handlers for gimel.event.emit in remote python code.
        this.gimel_events = {};
        // promises that are resolved once return value of a remote
        // function is received.
        this.result_router = {};

        this.gimel_log_handler = (data)=> {
            var msg = "gimellog "+data["timestamp"]+" "+
                data["severity"]+" "+data["msg"];
            console.log(msg);
        };
        this.gimel_exc_handler = (data)=> {
            var msg = "Remote Exception\n"+data['stack_trace'];
            console.log(msg);
            alert(msg);
        };
        // placeholder to allow user to capture all events
        this.gimel_all_event_handler = (data)=> {}; 

        this.gimelId = gimelId;
        this.apiKey = apiKey;

         
    }

    setupEvtHandlers() {
        var msg_handlers = {};

        var topic_prefix = "/api/gimelhost/" + this.gimelId;
        var topic_log    = topic_prefix + "/log";
        var topic_result = topic_prefix + "/result";
        var topic_event  = topic_prefix + "/event";
        var topic_exc    = topic_prefix + "/exception"; 

        // setup default handlers for events
        msg_handlers[topic_log] = (mqtt_msg)=> {
            var data = JSON.parse(mqtt_msg.payloadString);
            this.gimel_log_handler(data);
        };
 
        msg_handlers[topic_event] = (mqtt_msg)=> {
            var data = JSON.parse(mqtt_msg.payloadString);
            var handler = this.gimel_events[data.key] || null; 
            if (handler !== null) {
                try {
                    handler(data.timestamp, data.value);
                } catch( e ) {
                    console.error(e);
                }
            }
            try {
                this.gimel_all_event_handler(data);
            } catch(e) {
                console.error(e);
            }
        };
        msg_handlers[topic_result] = (mqtt_msg)=> {
            var data = JSON.parse(mqtt_msg.payloadString);
            var evtName = 'gimel-'+data.msgId;
            const event = new CustomEvent(evtName, { detail: data });
            // route event to promise.
            document.body.dispatchEvent(event);    
            // gets routed to event handler recreated within the promise
            // inside the _call() command. 
            // document.body.addEventListener('gimel-<msgId>', <promise.resolve>, false);
        };
        msg_handlers[topic_exc] = (mqtt_msg)=> {
            var data = JSON.parse(mqtt_msg.payloadString);
            try {
                this.gimel_exc_handler(data);
            } catch(e) {
                console.error(e);
            }  
        }; 

        return msg_handlers;
    } 

    register(evt, cb) {
        this.gimel_events[evt] = cb; 
    }

    unregister(evt) {
        if (typeof this.gimel_events[evt] !== 'undefined') {
            delete this.gimel_events[evt];
        }
    }


    /* primitive for call module and call git */
    _call(path, modname, objname, args)
    {
        return new Promise((resolution,rejection) => {
            var url = "/api/gimel/run";
            var msgId = uid();

            var evtId = 'gimel-'+msgId;
            // handle the result event.
            var evthandler = (e)=>{
                var data = e.detail;
                var error = data.error || null;

                if (error !== null) {
                    rejection(error);
                } else {
                    // route result back to caller.
                    resolution(data.result);
                }
                // dispose of this handler 
                document.body.removeEventListener(evtId,evthandler);    
            };
            document.body.addEventListener(evtId, evthandler, false);

            var data = {
                "apiKey": this.apiKey,
                "gimelId": this.gimelId,
                "modname": modname,
                "path": path,
                "objname": objname,
                "args": args,
                "msgId": msgId,
                "json": true
            }; 

            $.ajax({
                url:url, 
                data:JSON.stringify(data), 
                type:"post", 
                contentType:"application/json", 
                dataType:"json"
            }).fail((xhr, status, error) => {
                if (xhr.status !== 200) {
                    var errMsg = xhr.statusText;
                    try {
                        errMsg = xhr.responseText.split('<p>')[1].split("</p>")[0];
                    } finally {
                        errMsg.replace("&#x27;","'")

                        var errorMessage = xhr.status + ': ' + errMsg;
                        alert('Error - ' + errorMessage);
                    } 
                }
            });
        });
    }

    /*
        have the module loaded in the gimel without execution this allows
        for 'import ...' statements to be executed speeding up subsiquent
        calls to callmodule

        Note: this generates $SYS/import_cache user events that can be logged

        returns the elapsed time to execute 'import ...' statements 
    */
    cacheImportModule(modname) 
    {
        return this.callmodule(modname, "__import_cache__"); 
    }

    callmodule(modname, funcname, ...args) 
    {
        return this._call("/modules", modname, funcname, args);
    } 

    /*
        have the module loaded in the gimel without execution this allows
        for 'import ...' statements to be executed speeding up subsiquent
        calls to callgitmodule

        Note: this generates $SYS/import_cache user events that can be logged

        returns the elapsed time to execute 'import ...' statements 
    */
    cacheImportGitModule(reponame, path_within_repo, modname)
    {
        return this.callgitmodule(reponame, path_within_repo, modname, "__import_cache__");  
    }

    callgitmodule(reponame, path_within_repo, modname, funcname, ...args)
    {
        var path = "/git/"+reponame;
        
        if (path_within_repo.length > 0) {
            path += "/"+path_within_repo;
        } 
        return this._call(path, modname, funcname, args);
    }

}


class MenshnetClient
{
    constructor(mqtt_client, apiKey) {
        this.mqtt_client = mqtt_client;
        this.apiKey = apiKey;
        this.gimels = {};
        this.msg_handlers = {};
        this.heartbeat_timer = null;
    }

    createHeartbeatTimerIfNotExists() {
	var heartbeat_url = "/api/gimel-heartbeat?apiKey="+this.apiKey;    
        if (this.heartbeat_timer === null) {
            // keep alive messages for running gimels
            // start a background timer loop.
            this.heartbeat_timer = setInterval(()=>{
                var glist = Object.keys(this.gimels);		
                if (glist.length > 0) {
                    $.ajax({
                       url: heartbeat_url,
                       type: "POST",
                       data: JSON.stringify({
                           gimelIdList: glist
                       }),
                       contentType: "application/json; charset=utf-8",
                       dataType: "json"
                    });
                }
            }, 7500);
     
	        $(window).on('beforeunload', ()=> {
                try {   
                    this.heartbeat_timer.clearInterval();
                } catch(e) {
                }
            }); 
        }
    }


    // shut down remote process
    shutdown(gimelInst) {
        var gimelId = gimelInst.gimelId;
        var gimel = this.gimels[gimelId];
        if (typeof gimel !== 'undefined') {
            delete this.gimels[gimelId];
            var url = "/api/gimel?gimelId="+gimelId+"&apiKey="+this.apiKey;
            $.ajax({
                url,
                type: 'DELETE'
            });
        }   
    }

    /**
        Inbound message to be routed to a callback. 
    */
    on_message(mqtt_msg) {
        var topic = mqtt_msg.destinationName;
        var func = this.msg_handlers[topic] || null;
        if (func !== null) { 
            func(mqtt_msg);
        } 
    }


    // configures a gimel given a valid gimelId.
    configureGimel(gimelId) {
        return new Promise((resolution,rejection) => {
            // interface object for remote process 
            var gimel = new Gimel(gimelId,this.apiKey);

            // event handlers to route inbound messages to GUI
            var handlers = gimel.setupEvtHandlers();

            // subscribe to all topics
            var topics = Object.keys(handlers);
            var unsubcount = topics.length;
             
            topics.forEach((topic)=>{
                var subscribe_cb = {
                    onSuccess: ()=>{      
                        // assign topic handler to callback lookup table
                        this.msg_handlers[topic] = handlers[topic];
                        // decrement the count of unsubscribed topics
                        unsubcount--;

                        // have we subscribed to all ?
                        if (unsubcount === 0) {
                            // we are done and the gimel is operational 
                            this.gimels[gimelId] = gimel;
                            // start heartbeat timer if needed
                            this.createHeartbeatTimerIfNotExists();
                            // pass to caller promise fullfilled.
                            resolution(gimel);
                        }
                    },
                    onFailure: (context,ecode,emsg)=>{
                        var e = "MQTT error:"+ecode+" "+emsg;
                        rejection(e);
                    }
                }; 

                // ubscribe to topic
                this.mqtt_client.subscribe(
                    topic, subscribe_cb); // end subscribe
            }); // end foreach    
        });
    }


    // get a list of gimels that are already running on the
    // system. This is so we can monitor existing gimels running
    // on other instances of the menshnet client from other systems.
    activeGimels() {
        return new Promise((resolution,rejection) => {
           var url = "/api/activeGimels?apiKey="+this.apiKey;
           $.get(url, (gimelIdList) => {
               var gimels = [];
               gimelIdList.forEach((gimelId) => {
                   this.configureGimel(gimelId)
                   .then( (gimel)=> {
                       gimels.push(gimel); 
                       // have we gotten all the gimels and set them up?
                       if (gimels.length === gimelIdList.length) {
                           resolution(gimels);
                       } 
                   }, rejection );
               });

           }).fail(()=>{ rejection("Server error: unable to get active gimels"); }); 
        });
    }

    // launch a new gimel 
    launch() {
        // return a promise to be fullfilled once remote process is up.
        return new Promise((resolution,rejection) => {
            // ajax call to remote server, trigger a new process in the cluster
            // return the gimelId as a handle for operations.
            var url = "/api/gimel?apiKey="+this.apiKey;
            $.get(url, (gimelId)=>{
                this.configureGimel(gimelId).then(resolution,rejection);
            }).fail(()=>{ rejection("Server error: unable to launch gimel"); });                        
        });
    } 
}

/**
   Root object that creates a MenshnetClient upon a successful 
   connect.
*/
class Menshnet
{
    constructor(apiKey) {
        this.apiKey = apiKey;
    }

    connect() {
        // attempt to connect and subscribe to topics.
        // call resolution when setup.
        return new Promise( (resolution,rejection) => {
            var clientId = Math.random().toString(16).substr(2, 16);
            var mqtt_client = new Paho.MQTT.Client("", 0, clientId);
            var mc = new MenshnetClient(mqtt_client, this.apiKey);

            mqtt_client.onMessageArrived = (mqtt_msg) => {
                mc.on_message(mqtt_msg);
            };

            mqtt_client.onConnected = ()=> {
                console.log("------MQTT CONNECTED--------");
                resolution(mc);
            };

            

            // determine useSSL/uri using the window location.
            var loc = window.location.toString();
            var mqtt_proto = loc.startsWith('https') ? 'wss':'ws';
            var mqtt_host = loc.split('/')[2];
            var mqtt_url = mqtt_proto+"://"+mqtt_host+"/mqtt";

	        console.log("connecting to "+mqtt_url);	
       		 
            var copts = { 
                hosts: [
                    mqtt_url
                ],
                useSSL: mqtt_proto == 'wss',
                onFailure: (context,ecode,estr)=>{
                    console.log(estr);
                    rejection();
                },
                onSuccess:()=>{
                    resolution(mc);
                },
                reconnect: true
            };
            mqtt_client.connect(copts);


            /** not needed now that reconnect flag exists.
            // try to reconnect
            mqtt_client.onConnectionLost = (ecode,emsg)=>{
                console.error("onConnectionLost:"+ecode+" "+emsg);
                // reconnect attempt.
                setTimeout(()=>{mqtt_client.connect(copts);}, 2000);
            };
            */



        }); 
    }

}

 


